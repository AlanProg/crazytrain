﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSource_VolumeBoost : MonoBehaviour
{
    [SerializeField] private float volume;
    // Start is called before the first frame update
    void Start()
    {
        AudioSource temp = GetComponent<AudioSource>();
        
        temp.volume = volume;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
