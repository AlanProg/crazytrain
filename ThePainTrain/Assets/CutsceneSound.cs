﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneSound : MonoBehaviour
{
    public AudioClip grito;
    public AudioClip partida;
    public AudioClip predo1;
    public AudioClip predo2;
    public AudioClip predo3;
    public AudioClip romulo1;


    public AudioSource audioS;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Grito()
    {
        audioS.PlayOneShot(grito);
    }

    void Partida()
    {
        audioS.PlayOneShot(partida, 2.5f);

    }

    void Predo1()
    {
        audioS.PlayOneShot(predo1,2);
    }
    void Predo2()
    {
        audioS.PlayOneShot(predo2,2);
    }
    void Predo3()
    {
        audioS.PlayOneShot(predo3,2);
    }
    void Romulo1()
    {
        audioS.PlayOneShot(romulo1,2);
    }
}
