﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneDoorTemp : MonoBehaviour
{
    public int sceneIndex;

    private Image fade;

    private Text loading;

    private float alphaValue = 0;

    void Awake()
    {

        loading = GameObject.FindGameObjectWithTag("Loading").GetComponent<Text>();
        fade = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();

    }

    private void Interaction()
    {
        StartCoroutine("FadeIn");
    }

    private void NextLevel()
    {
        SceneManager.LoadScene(sceneIndex);
    }

    IEnumerator FadeIn()
    {
        if (fade.color.a < 1)
        {
            alphaValue = fade.color.a;
            alphaValue += 0.1f;
            fade.color = new Color(fade.color.r, fade.color.g, fade.color.b, alphaValue);
            yield return new WaitForSeconds(0.1f);
            StartCoroutine("FadeIn");
        }
        else
        {
            loading.enabled = true;
            NextLevel();
        }

    }

}
