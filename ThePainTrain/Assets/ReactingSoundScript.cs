﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactingSoundScript : MonoBehaviour
{



    [SerializeField] private AudioClip[] voz_trancada;
    [SerializeField] private AudioClip[] voz_surpresa;
    [SerializeField] private AudioClip[] voz_wrongthink;
    [SerializeField] private AudioClip[] voz_desespero;
    private AudioSource src;
    [SerializeField] private float volume,desesperoMod = 3;

    private bool desespero = false;

    public enum ReactionType
    {
        none,
        trancado,
        surpreso,
        wrongthink
    };

    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (desespero)
        {
            if (!src.isPlaying)
            {
                src.PlayOneShot(voz_desespero[Random.Range(0, voz_desespero.Length - 1)],desesperoMod);
            }
        }
    }

    void PlayReaction(string str)
    {
        switch (str) {
            case "none":
            break;
            case "trancado":
                StartCoroutine(PlaySoundWithPause(voz_trancada));
            break;
            case "surpreso":
                StartCoroutine(PlaySoundWithPause(voz_surpresa));
            break;
            case "wrongthink":
                StartCoroutine(PlaySoundWithPause(voz_wrongthink));
            break;
            case "desespero":
                if (desespero)
                {
                    desespero = false;
                }
                else
                {
                    desespero = true;
                  
                }
            break;
        }
    }

    

    IEnumerator PlaySoundWithPause(AudioClip[] clp)
    {
        yield return new WaitForSeconds (1f);
        src.PlayOneShot(clp[Random.Range(0, clp.Length-1)],volume);
        
    }

}
