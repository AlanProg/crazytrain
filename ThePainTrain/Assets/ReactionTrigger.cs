﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionTrigger : MonoBehaviour
{

    [Header("Vai disparar alguma reação da protagonista")]
    [SerializeField] ReactingSoundScript.ReactionType reactionTrigger = ReactingSoundScript.ReactionType.none;
    private ReactingSoundScript reactions;

    private bool hasPlayed = false;

    // Start is called before the first frame update
    void Start()
    {
        reactions = GameObject.FindGameObjectWithTag("PlayerAudioFalas").GetComponent<ReactingSoundScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!hasPlayed)
            {
                print("aaaaaa");
                reactions.SendMessage("PlayReaction", reactionTrigger.ToString());
                hasPlayed = true;
            }
        }
    }
}
