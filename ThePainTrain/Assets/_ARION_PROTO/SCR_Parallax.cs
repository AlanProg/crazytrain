﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCR_Parallax : MonoBehaviour
{
    public GameObject[] terrain;
    public float speed;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        terrain[0].transform.Translate(0, 0, speed);
        terrain[1].transform.Translate(0, 0, speed);
        if (terrain[1].transform.position.z >= 200)
        {
            terrain[1].transform.position = new Vector3(0, 0, 0);
        }

        if (terrain[0].transform.position.z >= 200)
        {
            terrain[0].transform.position = new Vector3(0, 0, 0);
        }
    }
}
